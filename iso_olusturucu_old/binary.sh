export pwd=$(pwd)
cd ..
if [ -f ./config/binary ]
then
  export binary=$(cat ./config/binary)
else
  export binary=$pwd/binary
fi
grub-mkrescue -o "live-image-$(date +%y%m%d%H%M%S).hybrid.iso" \
--sparc-boot -c 'iso9660' -- \
-as mkisofs \
-isohybrid-gpt-basdat \
-iso-level 3 -rock -joliet \
-max-iso9660-filenames -omit-period \
-omit-version-number -relaxed-filenames -allow-lowercase \
-no-emul-boot -boot-load-size 4 -boot-info-table \
-volid "CustomLiveIso"  $binary
